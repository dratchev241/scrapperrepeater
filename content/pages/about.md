---
title: About The Scrapper Repeater
description: 'About The ScrapperRepeater'
author: W2NAP
---
### VHF
* 147.15+ PL 151.4
* Motorola Quantar.

### UHF
* 442.275+ DMR
* ColorCode 7
* TS1 TG311852 Fulltime
* TS2 TG3118 TG31181 TG31183 Fulltime

### About
The Scrapper system currently has a 440 repeater and a 2 meter repeater in Anderson,IN. The 440 DMR repeater has an output of 442.275MHz and an input of 447.275MHz (5MHz offset +) and uses ColorCode 7 And the 2 meter repeater has an output of 147.15MHz and an input of 147.75Mhz (600KHz offset +) and uses a PL of 151.4. Both are currently as a test spot for the moment. Should be noted HT coverage is not great at the current time maybe a few miles from the broadway/scatterfield split.

The scrapper repeater got its start in 2005 when a few of us got tired of having other local repeaters getting turned off on us just cause we was having fun and decided hell with it we will just put our own damn repeater up. At one point the scrap repeater had over 1,000 people listening on radio ref something no other Indiana repeater could ever dream to do. So what is with the scrapper name? Well we are “scrappers” we are the guys who take stuff people will throw away fix it and use it instead of buying some made in commie china garbage. A lot of us have computers, televisions, radios and other things that we picked right off the trash bin fixed and continue to use said stuff.

Sadly over the last few years we have lost a lot of the old crew, but we are still here at least for now.
