+++
date = '2025-01-26T15:34:47-05:00'
draft = false
title = '2m Repeater Update'
tags = ['repeater']
isStarred = false

+++

### Update Status
Currently the 2 meter repeater is on a very low antenna, like 10ft above the ground. Thanks to the horrid wind this winter it knocked the antenna mast over to where it had a 45 degree lean on the top post and it was barely hanging on so I had to drop it down. I was able to stick the antenna on a 10ft pipe and clamp it to a fence post and lucky the antenna was not damaged (1.2VSWR on the TX and RX Freqs) so the repeater is on.

### Work to do
I have ordered a new cable harness for the duplexer and HOPEFULLY that will fix the horrid desense issue that plauges the repeater, I still won't be able to do anything with the power line noise interference. I also will at some point get the allstar controller hooked up to the quantar which will give echostink and allstar capability to it.
