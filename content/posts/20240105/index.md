+++
date = '2024-01-05T14:49:47-04:00'
draft = false
title = 'Repeater Updates Jan 5, 2024'
tags = ['ham radio']

+++

Just a quick update on the 2 meter and 440 repeaters, They are currently NOT linked together, and will not be linked together until much work is done to them. Currently have so many issues with equipment and over the next couple months we will begin to try and fix all the issues we have which is going to take some time.