+++
date = '2024-03-26T14:49:47-04:00'
draft = false
title = 'Repeater Updates Mar 26, 2024'
tags = ['ham radio']

+++

So what is the current status of things? Well nothing has changed at this time. The machines are still not linked together and I have noticed today the controller is giving the wrong time so maybe this weekend I will fix that.

What about the future? Well the 2 meter machine needs a LOT of work. Currently first on the doc is to fix the duplexers, I believe the main issue is the cable harness is not good. I have maybe sourced a decent replacement but that will cost about $300 then I will have to retune the cans. What is wrong you ask? I could not get the current setup to tune correctly as I get a double hump on the rejects. So the harness replacement is the next step. The choices are $300 to take a shot with a replacement harness or spend $800+ to send them off, and $300 is less then $800+ so that is what this poor group has to do.

The other MAJOR issue with 2 meter is power line interference, While you might say just bitch to the power company... eh might as well go outside and piss in the wind. When I brought the machine here I hadn't messed with 2 meter is so long I forgot about the horrid noise issue, and this is an issue that has been on going for 20+ years. So as long as the machine is at it's current location the power line noise issue is just going to kill it for the most part. So the machine will have to be moved at some point but again that means $$$$$$, $ we just do not have.

The 440 machine, Well the antenna is bad so that is a problem. UHF cans need work and honestly I do not like the current repeater. It is an RF Tech repeater and quite frankly it is meh. Yeah it works but I can not control CTCSS on RX/TX via control lines it HAS to be done with the repeater. And the audio quality of the machine is just meh. So... I will likely replace the rx and tx with some /\/\oto GM-300's where I can get ctcss logic lines out. (and have better audio quality)

The duplexer on the 440 needs work, again I suspect cable harness issue. Hopefully at some point in the year I can get another harness made up for it. it is also possible the duplexer is just damaged as it did have some slight damage when I got them so maybe need to replace the duplexer which means more $$$$$.

Allstar/Echolink Maybe at some point will come. Current controller just needs replaced it is way too glitchy. (it is a Arcom RC-210) IDK why but the thing is horridly glitchy so once the cash comes in it will be replaced with allstar/app_rpt for repeater controller duties.

TL;DR
VHF Quantar: Works fine.
VHF Duplexer: needs work, cable harness replacement and then re-tuned again.
VHF Issue: Power Line noise, can't do shit about it. So repeater will have to be moved somewhere else.
UHF Repeater: RX and TX exciter needs to be tossed in the garbage, replaced with some GM-300's or entire system replaced with a Quantar (Which is what I would like to do but need about $1400 for that, and I don't have that kind of $$$ just laying around)
UHF Antenna: Needs replaced, it is bad.
UHF Duplexer: Cable harness, re-tuned. maybe replaced.

Now if someone was to say just donate about $10,000 I could then build a real real nice system.