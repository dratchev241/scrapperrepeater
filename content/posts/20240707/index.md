+++
date = '2024-07-07T14:49:47-04:00'
draft = false
title = 'Future Updates'
tags = ['repeater', 'dmr']
description = 'DMR Future change and 2 meter work.'
+++

In the near future the analog 440 machine will go offline and be converted to DMR, Not sure exactly when this changeover will happen but likely in the next month or so. We will also begin to work on the 2 meter machine and make it much much better. The Allstar/Echostink will be on the 2 meter machine. Still have much work to do on the machines to get this done but hopefully I can have this all done by the end of August.

Also two hamfests coming up in July. 7/20 is Auburn Indiana and 7/27 is the ECI over in Portland Indiana. check the hamfests page for info.