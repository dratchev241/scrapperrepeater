+++
date = '2023-12-12T14:49:47-04:00'
draft = false
title = '2024 Hamfests List'
tags = ['hamfests']

+++

### 02/10/2024 - Hendricks County Hamfest
* Location: Danville, IN
* Address: Hendricks County 4h Fairgrounds and Conference Center
* 1900 E. Main Street
* Danville, IN 46122
* Talk-In: 147.015+ 88.5 CTCSS
* Sponsor: Hendricks Country Amateur Radio Society
* Website: [http://n9hc.org](http://n9hc.org)

### 02/24/2024 - Cabin Fever Hamfest
* Location: La Porte, IN
* Address: La Porte Civic Auditorium
* 1001 Ridge St.
* La Porte, IN 46350
* Talk-in: 146.61- 131.8 PL
* Sponsor: La Porte County Amateur Radio Club
* Website: [https://lpcarc.org/hamfest/](https://lpcarc.org/hamfest/)

### 02/24/2024 - Dugger Amateur Radio Club Hamfest
* Location: Dugger, IN
* Address: Dugger Comm. Building
* 840 Hicum St.
* Dugger, IN 47848
* Talk-In: 146.775+ pl 136.5
* Sponsor:

### 05/04/2024 - NCI HAMFEST
* `NOTE THE LOCATION CHANGE!`
* Location: Marion, IN
* Address: Hart's Celebration Center
* 3031 E. 450 N
* Marion, IN
* Talk-In: 147.345+ 131.8PL
* Sponsor:
* Website: [https://www.ncihamfest.com/](https://www.ncihamfest.com/)

### 05/17-19/2024 - Dayton Hamvention
* Location: Dayton, OH
* Address: Greene County Expo Center
* 120 Fairground Rd
* Xenia, OH 45385
* Talk-In: 146.94- 123.0 PL
* Sponsor: Dayton Amateur Radio Association
* Website: [https://hamvention.org/](https://hamvention.org/)

### 07/20/2024 - Auburn Hamfest
* Location: Auburn, IN
* Address: Auburn Cord Duesenberg Museum
* 1600 Wayne Street
* Auburn, IN 46706
* Talk-In: 147.015+ 141.3pl
* Sponsor: Northeastern Indiana Amateur Radio Association
* Website: [http://w9ou.org](http://w9ou.org)

### 07/21/2024 - Van Wert Hamfest
* Location: Van Wert, OH
* Address: Van Wert County Fairground
* 1055 S. Washington Street
* Van Wert, OH 45891
* Talk-In: 146.850-
* Sponsor: Van Wert Amateur Radio Club
* Website: [http://w8fy.org](http://w8fy.org)

### 07/27/2024 - East Central Indiana HAMFEST
* Location : Jay County Fairgrounds Portland, IN
* Address: 806 E Votaw St, Portland, Indiana 47375
* Talk-In: 147.300+ 127.3pl
* Website: [ECIHamfest](https://sites.google.com/view/ecindianahamfest/home)

### 08/03/2024 - Elkhart East Hamfest
* Location: Elkhart, IN
* Address: Northern Indiana Event Center's Orthwein Pavilion
* 21565 Executive Pkwy
* Elkhart, IN 46514
* Talk-In: 147.330+ PL 131.8
* Sponsor:
* Website: [https://elkharteasthamfest.com/](https://elkharteasthamfest.com/)

### 08/10/2024 - Cincinnati Hamfest
* Location: Owensville, OH
* Address:Clermont County Fairgrounds
* 1000 Locust St.
* Owensville, OH 45160
* Talk-In: 147.345+ PL 123.0
* Sponsor: Milford ARC
* Website: [https://CincinnatiHamfest.org](https://CincinnatiHamfest.org)

### 08/10/2024 - Hendricks County Tailgate Fest
* Location: Avon, IN
* Address: Avon United Methodist Church
* 6850 E. US Hwy 36
* Avon, IN 46123
* Talk-In: 145.130- PL88.5
* Sponsor: Ham Emergency Radio Operations

### 08/11/2024 - ARANCI Hamfest
* Location: Lake Village, IN
* Address: Lake Village Community Center
* 9728 North 300 West
* Lake Village, IN 46349
* Talk-In: 145.330- pl 131.8
* Sponsor: ARANCI

### 10/12/2024 - Hoosier Hills Hamfest
* Location: Bedford, IN
* Address: Lawrence County 4-H Fairgrounds
* 11265 US-50
* Bedford, IN 47421
* Talk-In: 146.73- PL/107.2
* Sponsor: Hoosier Hills Ham Club
* Website: [https://hoosierhillshamfest.com](https://hoosierhillshamfest.com)

### 10/19/2024 - Shelbyville Tailgate
* Location: Shelbyville, IN
* Address: Shelby County Fairgrounds
* 500 Frank St
* Shelbyville, IN 46176
* Talk-In: 145.48- 88.5pl
* Website: [https://brvars.com/](https://brvars.com/)

### 11/16-17/2024 - Ft Wayne Hamfest
* Location: Fort Wayne, IN
* Address: Allen County War Memorial Coliseum
* 4000 Parnell Ave
* Fort Wayne, IN 46805
* Talk-In: 146.88-
* Website: [http://www.acarts.com/hfmain.htm](http://www.acarts.com/hfmain.htm)