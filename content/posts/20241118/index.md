+++
date = '2024-11-18T14:07:47-05:00'
draft = false
title = '2025 Hamfests List'
tags = ['hamfests']
isStarred = true

+++

### 02/08/2025 - Hendricks County Hamfest
* Location: Danville, IN
* Address: Hendricks County 4h Fairgrounds and Conference Center
* 1900 E. Main Street
* Danville, IN 46122
* Talk-In: 147.015+ 88.5 CTCSS
* Sponsor: Hendricks Country Amateur Radio Society
* Website: [http://n9hc.org](http://n9hc.org)

### 02/22/2025 - Cabin Fever Hamfest
* `NOTE THE LOCATION CHANGE!`
* Location: La Porte, IN
* Address: LaPORTE COUNTY FAIR GROUNDS
* 2581 W. State Rd. 2
* La Porte, IN 46350
* Talk-in: 146.61- 131.8 PL
* Sponsor: La Porte County Amateur Radio Club
* Website: [https://lpcarc.org/hamfest/](https://lpcarc.org/hamfest/)

### 02/22/2025 - Dugger Amateur Radio Club Hamfest
* Location: Dugger, IN
* Address: Dugger Comm. Building
* 840 Hicum St.
* Dugger, IN 47848
* Talk-In: 146.775+ pl 136.5
* Sponsor:

### 03/22/2025 WVARA Hamfest
* Location: Brazil, IN
* Address: Clay County Fairgrounds
* 6550 N SR 59
* Brazil, IN
* Talk-In 146.685- 151.4PL
* Sponsor: Wabash Valley ARA
* Website: [W9UUU.org](https://www.w9uuu.org/hamfest.php)

### 04/05/2025 Columbus Hamfest
* Location: Columbus, IN
* Address: North High School Cafeteria
* 1400 25th St
* Columbus, IN 47201
* Sponsor: Columbus Amateur Radio Club
* Website: [carcnet.net](http://carcnet.net)

### 04/26/2025 - NCI HAMFEST
* `NOTE THE LOCATION CHANGE!`
* Location: Peru, IN
* Address: Miami County Fairgrounds
* 1029 W 200 N
* Peru, IN 46970
* Talk-In: 147.345+ 131.8PL
* Sponsor:
* Website: [https://www.ncihamfest.com/](https://www.ncihamfest.com/)

### 05/16-18/2025 - Dayton Hamvention
* Location: Dayton, OH
* Address: Greene County Expo Center
* 120 Fairground Rd
* Xenia, OH 45385
* Talk-In: 146.94- 123.0 PL
* Sponsor: Dayton Amateur Radio Association
* Website: [https://hamvention.org/](https://hamvention.org/)

### 07/19/2025 - Auburn Hamfest
* Location: Auburn, IN
* Address: Auburn Cord Duesenberg Museum
* 1600 Wayne Street
* Auburn, IN 46706
* Talk-In: 147.015+ 141.3pl
* Sponsor: Northeastern Indiana Amateur Radio Association
* Website: [http://w9ou.org](http://w9ou.org)

### 07/20/2025 - Van Wert Hamfest
* Location: Van Wert, OH
* Address: Van Wert County Fairground
* 1055 S. Washington Street
* Van Wert, OH 45891
* Talk-In: 146.850-
* Sponsor: Van Wert Amateur Radio Club
* Website: [http://w8fy.org](http://w8fy.org)

### 07/26/2025 - East Central Indiana HAMFEST
* Location : Jay County Fairgrounds Portland, IN
* Address: 806 E Votaw St
* Portland, Indiana 47375
* Talk-In: 147.300+ 127.3pl
* Website: [ECIHamfest](https://sites.google.com/view/ecindianahamfest/home)

### 08/02/2025 - Elkhart East Hamfest
* Location: Elkhart, IN
* Address: Northern Indiana Event Center's Orthwein Pavilion
* 21565 Executive Pkwy
* Elkhart, IN 46514
* Talk-In: 147.330+ PL 131.8
* Sponsor:
* Website: [https://elkharteasthamfest.com/](https://elkharteasthamfest.com/)

### 08/09/2025 - Cincinnati Hamfest
* Location: Owensville, OH
* Address: Clermont County Fairgrounds
* 1000 Locust St.
* Owensville, OH 45160
* Talk-In: 147.345+ PL 123.0
* Sponsor: Milford ARC
* Website: [https://CincinnatiHamfest.org](https://CincinnatiHamfest.org)

### 08/09/2025 - Hendricks County Tailgate Fest
* Location: Avon, IN
* Address: Avon United Methodist Church
* 6850 E. US Hwy 36
* Avon, IN 46123
* Talk-In: 145.130- PL88.5
* Sponsor: Ham Emergency Radio Operations

### 08/10/2025 - ARANCI Hamfest
* Location: Lake Village, IN
* Address: Lake Village Community Center
* 9728 North 300 West
* Lake Village, IN 46349
* Talk-In: 145.330- pl 131.8
* Sponsor: ARANCI

### 10/11/2025 - Hoosier Hills Hamfest
* Location: Bedford, IN
* Address: Lawrence County 4-H Fairgrounds
* 11265 US-50
* Bedford, IN 47421
* Talk-In: 146.73- PL/107.2
* Sponsor: Hoosier Hills Ham Club
* Website: [https://hoosierhillshamfest.com](https://hoosierhillshamfest.com)

### 10/18/2025 - Shelbyville Tailgate
* `NEEDS Confirmed`
* Location: Shelbyville, IN
* Address: Shelby County Fairgrounds
* 500 Frank St
* Shelbyville, IN 46176
* Talk-In: 145.48- 88.5pl
* Website: [https://brvars.com/](https://brvars.com/)

### 11/15-16/2025 - Ft Wayne Hamfest
* Location: Fort Wayne, IN
* Address: Allen County War Memorial Coliseum
* 4000 Parnell Ave
* Fort Wayne, IN 46805
* Talk-In: 146.88-
* Website: [http://www.acarts.com/hfmain.htm](http://www.acarts.com/hfmain.htm)

### 11/29/2025 WVARA Turkeyfest
* Location: Brazil, IN
* Address: Clay County Fairgrounds
* 6550 N SR 59
* Brazil, IN
* Talk-In 146.685- 151.4PL
* Sponsor: Wabash Valley ARA
* Website: [W9UUU.org](https://www.w9uuu.org/hamfest.php)
