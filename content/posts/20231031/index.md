+++
date = '2023-10-31T14:49:47-04:00'
draft = false
title = 'Fort Wayne Hamfest 2023'
tags = ['hamfests']

+++

Fort Wayne Hamfest

Nov 18-19 2023
at the Allen County War Memorial Coliseum.
Address:
4000 Parnell Ave
Fort Wayne, IN 46805

Hamfest website [http://acarts.com/hfmain.htm](http://acarts.com/hfmain.htm)