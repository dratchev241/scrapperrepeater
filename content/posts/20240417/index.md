+++
date = '2024-04-17T14:49:47-04:00'
draft = false
title = '04/17/24 Update'
tags = ['ham radio']

+++

Quick blurp on the repeater deal, I have moved the 440 machine over to allstar/app_rpt (node 42145) it is working. However range is still limited as the equipment still needs work (antenna replacement and duplexers worked on). The 2 meter machine is currently down I am not sure how long it will be down for as I do need to get the duplexers on it set correctly which means I do need a new cable harness and they do happen to cost $$$$. As well need to get a interface card for it for allstar/app_rpt and I am unsure how long that will take hopefully can get that part done in the next month.