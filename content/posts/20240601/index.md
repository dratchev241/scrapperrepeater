+++
date = '2024-06-01T14:49:47-04:00'
draft = false
title = 'Quick Status Update'
tags = ['ham radio']

+++

Everything is still on a holding pattern, I did turn the 2 meter machine back on and it is running off the internal quantar controller. Allstar/Echostink is on the 440 machine. Both machines still have major issues which limit useability.

The plan is to work on this crap over the summer but I have to have a few things to actually move (aka sell) before I can do anything. The plan is to replace part of the 440 machine to a better rx/tx exciter likely a Kenwood TKR-840 and use the 100W amp from the old machine and sell off the old rx and exciter modules.

The 2 meter machine still needs duplexer work as the cable harness for sure needs to be replaced. Also looking to get new antennas for the vhf and uhf systems

Along with all that, looking to move the systems somewhere around the area at a higher level site hopefully around 400ft+ if possible and with that we will need at least 500ft of 7/8 (ldf5-50a) heliax

Overall the cost to do everything is likely going to be $5,000+ so... maybe it will happen maybe not. Of course if you toss me some cash it can make it a bigger possibility of it actually happening.