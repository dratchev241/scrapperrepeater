+++
date = '2023-11-10T14:49:47-04:00'
draft = false
title = 'Turkeyfest Hamfest 2023'
tags = ['hamfests']

+++

Wabash Valley Amateur Radio Association Turkeyfest hamfest is November 25th 8am to noon in Brazil Indiana

Clay County 4-H Fairgrounds
6550 North State Road 59
Brazil, IN

[W9UUU](https://www.w9uuu.org/hamfest.php)